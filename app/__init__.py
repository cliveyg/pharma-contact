from flask import Flask
#from flask_sqlalchemy import SQLAlchemy

from app.extensions import db
from app.config import Config

import logging
from logging.handlers import RotatingFileHandler

def create_app(config_class=Config):

    app = Flask(__name__)
    # set app configs
    app.config.from_object(config_class)

    db.init_app(app)
    
    from app.main import bp as main_bp
    app.register_blueprint(main_bp)

    # register custom errors

    # logging stuff
    formatter = logging.Formatter("[%(asctime)s] [%(pathname)s:%(lineno)d] %(levelname)s - %(message)s")
    handler = RotatingFileHandler(app.config['LOG_FILENAME'], maxBytes=10000000, backupCount=5)
    log_level = app.config['LOG_LEVEL']

    if log_level == 'DEBUG': # pragma: no cover
        app.logger.setLevel(logging.DEBUG) # pragma: no cover
    elif log_level == 'INFO': # pragma: no cover
        app.logger.setLevel(logging.INFO) # pragma: no cover
    elif log_level == 'WARNING': # pragma: no cover
        app.logger.setLevel(logging.WARNING) # pragma: no cover
    elif log_level == 'ERROR': # pragma: no cover
        app.logger.setLevel(logging.ERROR) # pragma: no cover
    else: # pragma: no cover
        app.logger.setLevel(logging.CRITICAL) # pragma: no cover

    handler.setFormatter(formatter)
    app.logger.addHandler(handler)

    return app

from app import models

