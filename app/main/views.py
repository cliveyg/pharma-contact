# app/main/views.py
from app import db
from flask import jsonify, request, abort, redirect
from flask import current_app as app
from app.main import bp
from app.models import Contact
from app.assertions import assert_valid_schema
from jsonschema.exceptions import ValidationError as JsonValidationError
from sqlalchemy.exc import SQLAlchemyError, DBAPIError

# reject any non-json requests
@bp.before_request
def only_json():
    if not request.is_json:
        abort(400)

# -----------------------------------------------------------------------------
# returns a list of all contacts

@bp.route('/pharma/contact', methods=['GET'])
def list_contacts():

    contacts = []
    try:
        results = Contact.query.all()
    except SQLAlchemyError as err: # pragma: no cover
        app.logger.error("Error in list_contacts [%s]", str(err))
        return jsonify({ 'message': 'something went wrong' }), 500

    # not paginated - easy to paginate if need be
    for contact in results:
        contact_data = {}
        contact_data['username'] = contact.username
        contact_data['email'] = contact.email
        contact_data['firstname'] = contact.firstname
        contact_data['surname'] = contact.surname
        contacts.append(contact_data)

    # returns empty list if nothing found
    return jsonify({ 'contacts': contacts }), 200


# -----------------------------------------------------------------------------
# returns an individual contact

@bp.route('/pharma/contact/<username>', methods=['GET'])
def get_one_contact(username):

    message, code, contact = _get_and_check_contact(username) 
    
    if code != 200:
        return jsonify(message), code

    # i prefer to explicitly assign my outputs so i don't accidentally 
    # expose any restricted data
    output = {}
    output['username'] = contact.username
    output['email'] = contact.email
    output['firstname'] = contact.firstname
    output['surname'] = contact.surname

    return jsonify(output), 200


# -----------------------------------------------------------------------------
# creates a contact

@bp.route('/pharma/contact', methods=['POST'])
def create_contact():

    # check input is valid json
    try:
        data = request.get_json()
    except: # pragma: no cover
        return jsonify({ 'message': 'Check ya inputs mate. Yer not valid, Jason'}), 400

    # validate input against a json schema
    try:
        assert_valid_schema(data, 'contact')
    except JsonValidationError as error:
        app.logger.debug("The error is [%s]", str(error))
        #TODO: Better error message for user
        message = "Check your inputs."
        return jsonify({ 'message': message }), 400

    new_contact = Contact(email = data['email'],
                          username = data['username'],
                          surname = data['surname'],
                          firstname = data['firstname'])

    try:
        db.session.add(new_contact)
        db.session.flush()
        db.session.commit()
    except (SQLAlchemyError, DBAPIError) as err:
        error = str(err)
        db.session.rollback()
        app.logger.warning(error)
        if "UNIQUE" in error:
            error = "Duplicate username or email"
            code = 409
        else: # pragma: no cover
            error = "Something went wrong"
            code = 500
        return jsonify({ 'message': 'oopsy, sorry we couldn\'t complete your request',
                         'error': error }), code

    message = 'Contact ['+new_contact.username+'] created successfully'
    return jsonify({ 'message': message }), 201


# -----------------------------------------------------------------------------
# deletes a contact

@bp.route('/pharma/contact/<username>', methods=['DELETE'])
def delete_contact(username):

    # very basic data sanitation
    try:
        clean_username = str(username)
    except: # pragma: no cover
        return jsonify({ 'message': 'check your inputs' }), 400
    if len(clean_username) < 1 or len(clean_username) > 30:
        return jsonify({ 'message': 'username length incorrect' }), 400

    try:
        result = Contact.query.filter(Contact.username == username)\
                              .delete()
        db.session.flush()
        db.session.commit()
    except SQLAlchemyError as err: # pragma: no cover
        db.session.rollback()
        app.logger.error("Error in delete_contact [%s]", str(err))
        return jsonify({ 'message': 'something went wrong' }), 400

    if result:
        return '', 204

    return jsonify({ 'message': 'nope sorry, that\'s not happening today' }), 401


# -----------------------------------------------------------------------------
# edit an individual contact

@bp.route('/pharma/contact/<username>', methods=['PUT'])
def edit_contact(username):

    # check input is valid json
    try:
        input_data = request.get_json()
    except: # pragma: no cover
        return jsonify({ 'message': 'Check ya inputs mate. Yer not valid, Jason'}), 400

    # validate input against a json schema
    try:
        assert_valid_schema(input_data, 'contact_edit')
    except JsonValidationError as error:
        app.logger.debug("The error is [%s]", str(error))
        message = "Check your inputs. Error is: "+str(error)
        return jsonify({ 'message': message }), 400

    message, code, contact = _get_and_check_contact(username) 

    if code != 200:
        return jsonify(message), code

    # not sure a contact should be allowed to change their username but i'll allow it
    # this is also a very ugly way to set attributes
    move = False
    if input_data['username']:
        if input_data['username'] != contact.username:
            move = True
        contact.username = input_data['username']
    if input_data['email']:
        contact.email = input_data['email']
    if input_data['surname']:
        contact.surname = input_data['surname']
    if input_data['firstname']:
        contact.firstname = input_data['firstname']

    try:
        db.session.flush()
        db.session.commit()
    except (SQLAlchemyError, DBAPIError) as err: # pragma: no cover
        # could check for unique usernames, emails etc in error if needs be
        db.session.rollback()
        app.logger.error("Error in update_contact [%s]", str(err))
        return jsonify({ 'message': 'something went wrong' }), 400

    # i prefer to explicitly assign my outputs so i don't accidentally
    # expose any restricted data
    output = {}
    output['username'] = contact.username
    output['email'] = contact.email
    output['firstname'] = contact.firstname
    output['surname'] = contact.surname

    if move:
        return redirect("/pharma/contact/"+contact.username, code=302)

    return jsonify(output), 200


# -----------------------------------------------------------------------------
# helper route - useful for checking status of api

@bp.route('/pharma/status', methods=['GET'])
def system_running():
    app.logger.info("Praise the FSM! The sauce is ready")
    return jsonify({ 'message': 'System running...' }), 200


# -----------------------------------------------------------------------------
# route for anything left over - generates 404

@bp.route('/pharma/<everything_left>', methods=['GET', 'POST', 'PUT', 'DELETE'])
def not_found(everything_left):
    message = 'resource ['+everything_left+'] not found'
    return jsonify({ 'message': message }), 404


# -----------------------------------------------------------------------------
# helper method for getting and checking contact info. maybe move to another 
# module and tidy it up - it's pretty ugly

def _get_and_check_contact(username):

    # very basic data sanitization
    try:
        clean_username = str(username)
    except: # pragma: no cover
        return { 'message': 'check your inputs' }, 400, None
    if len(clean_username) < 1 or len(clean_username) > 30:
        return { 'message': 'username length incorrect' }, 400, None

    contact = None
    try:
        contact = Contact.query.filter_by(username = clean_username).first()
    except: # pragma: no cover
        return { 'message': 'oopsy, sorry we couldn\'t complete your request' },\
               500, None
    if not contact:
        message = "no contact found for supplied username ["+username+"]"
        return { 'message': message }, 404, None

    return None, 200, contact
