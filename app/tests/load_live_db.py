from .fixtures import addTestContacts
from flask import jsonify # pragma: no cover

# have to mock the require_access_level decorator here before it 
# gets attached to any classes or functions
from app import create_app, db # pragma: no cover
from app.config import TestConfig # pragma: no cover

from flask import current_app # pragma: no cover
from flask_testing import TestCase as FlaskTestCase # pragma: no cover


###############################################################################
####                      flask test case instance                         ####
###############################################################################

class MyTest(FlaskTestCase): # pragma: no cover

    ############################
    #### setup and teardown ####
    ############################

    def create_app(self):
        app = create_app()
        return app

    def setUp(self):
        db.drop_all()
        db.create_all()

    def tearDown(self):
        pass

###############################################################################
####                               tests                                   ####
###############################################################################

    def test_for_livedb(self): # pragma: no cover
        addresses = addTestContacts()
        self.assertTrue('sqlite://///home/clive/flask_apps/pharma/contacts.db' in 
                        self.app.config['SQLALCHEMY_DATABASE_URI'])

# -----------------------------------------------------------------------------

