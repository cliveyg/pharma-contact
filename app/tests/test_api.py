# app/tests/test_api.py
from .fixtures import addTestContacts
from flask import jsonify

from app import create_app, db
from app.models import Contact
from app.config import TestConfig

from flask import current_app 
from flask_testing import TestCase as FlaskTestCase

from sqlalchemy.exc import DataError
import os

###############################################################################
####                      flask test case instance                         ####
###############################################################################

class MyTest(FlaskTestCase):

    ############################
    #### setup and teardown ####
    ############################

    def create_app(self):
        app = create_app(TestConfig)
        return app

    def setUp(self):
        db.drop_all()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        os.remove(TestConfig.TEST_DB_PATH)


###############################################################################
####                               tests                                   ####
###############################################################################

    def test_for_testdb(self):
        self.assertTrue('sqlite:////home/clive/flask_apps/pharma/contacts-test.db' in 
                        self.app.config['SQLALCHEMY_DATABASE_URI'])

# -----------------------------------------------------------------------------

    def test_status_ok(self):
        headers = { 'Content-type': 'application/json' }
        response = self.client.get('/pharma/status', headers=headers)
        self.assertEqual(response.status_code, 200)

# -----------------------------------------------------------------------------

    def test_404(self):
        # this behaviour is slightly different to live as we've mocked the 
        headers = { 'Content-type': 'application/json' }
        response = self.client.get('/pharma/resourcenotfound', headers=headers)
        self.assertEqual(response.status_code, 404)

# -----------------------------------------------------------------------------

    def test_api_rejects_html_input(self):
        headers = { 'Content-type': 'text/html' }
        response = self.client.get('/pharma/status', headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------

    def test_contact_model_saves_ok(self):
        contact = Contact(username = "clive",
                          email = "clive@clive.is",
                          firstname = 'clive',
                          surname = 'glew')
        db.session.add(contact)
        db.session.commit()
        self.assertEqual(contact.id, 1)

# -----------------------------------------------------------------------------

    def test_return_list_of_contacts(self):
        contacts = addTestContacts() 
        headers = { 'Content-type': 'application/json' }
        response = self.client.get('/pharma/contact', headers=headers)
        results = response.json
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(results.get('contacts')), 4)

# -----------------------------------------------------------------------------

    def test_get_one_contact_ok(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        url = '/pharma/contact/'+contacts[0].username
        response = self.client.get(url, headers=headers)
        result = response.json
        self.assertEqual(response.status_code, 200)
        self.assertEqual(result.get('username'), contacts[0].username)
        self.assertEqual(result.get('email'), contacts[0].email)
        self.assertEqual(result.get('surname'), contacts[0].surname)
        self.assertEqual(result.get('firstname'), contacts[0].firstname)

# -----------------------------------------------------------------------------

    def test_get_fails_with_username(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        url = '/pharma/contact/1234567890123456789012345678901'
        response = self.client.get(url, headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------

    def test_get_fails_with_nonexistent_user(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        url = '/pharma/contact/invisibleman'
        response = self.client.get(url, headers=headers)
        self.assertEqual(response.status_code, 404)

# -----------------------------------------------------------------------------

    def test_delete_contact_ok(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        url = '/pharma/contact/'+contacts[0].username
        response = self.client.delete(url, headers=headers)
        self.assertEqual(response.status_code, 204)

# -----------------------------------------------------------------------------

    def test_delete_fails_with_username(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        url = '/pharma/contact/1234567890123456789012345678901'
        response = self.client.delete(url, headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------

    def test_delete_fails_with_username_that_does_not_exist(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        url = '/pharma/contact/whodis'
        response = self.client.delete(url, headers=headers)
        self.assertEqual(response.status_code, 401)

# -----------------------------------------------------------------------------

    def test_create_ok(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'username': 'testy01',
                        'email': 'testy01@gmail.com',
                        'firstname': 'firstname', 
                        'surname': 'testylast' }

        response = self.client.post('/pharma/contact', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 201)

# -----------------------------------------------------------------------------

    def test_create_fail_invalid_json(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        bad_string = '{ "bad" "json }'

        response = self.client.post('/pharma/contact', json=bad_string, headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------
# Could do a lot more tests around emails

    def test_create_fails_with_bad_email(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'username': 'testy01',
                        'email': 'testy01gmail.com',
                        'firstname': 'firstname', 
                        'surname': 'testylast' }

        response = self.client.post('/pharma/contact', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------

    def test_create_fails_with_extra_fields(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'username': 'testy01',
                        'email': 'testy01@gmail.com',
                        'firstname': 'firstname', 
                        'badfield' : 'nope',
                        'surname': 'testylast' }

        response = self.client.post('/pharma/contact', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 400)


# -----------------------------------------------------------------------------

    def test_create_fails_with_too_long_username(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'username': 'testy01testy01testy01testy01testy01testy01testy01',
                        'email': 'testy01@gmail.com',
                        'firstname': 'firstname',
                        'surname': 'testylast' }

        response = self.client.post('/pharma/contact', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------

    def test_create_fails_with_too_short_username(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'username': 't',
                        'email': 'testy01@gmail.com',
                        'firstname': 'firstname',
                        'surname': 'testylast' }

        response = self.client.post('/pharma/contact', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------

    def test_create_fails_with_too_long_email(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'username': 'too',
                        'email': 
                    'testy01testy01testy01testy01testy01testy01testy01@\
                            gmailgmailgmailgmailgmailgmailgmailgmailgma\
                            gmailgmailgmailgmailgmailgmailgmaililgmail.com',
                        'firstname': 'firstname',
                        'surname': 'testylast' }

        response = self.client.post('/pharma/contact', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------

    def test_create_fails_with_too_long_surname(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'username': 'too',
                        'email': 'testy01@gmail.com',
                        'firstname': 'firstname',
                        'surname': 'testylasttestylasttestylasttestylasttestylast\
                                testylasttestylasttestylast' }

        response = self.client.post('/pharma/contact', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------

    def test_create_fails_with_too_long_firstname(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'username': 'too',
                        'email': 'testy01@gmail.com',
                        'surname': 'surname',
                        'firstname': 'testylasttestylasttestylasttestylasttestylast\
                                testylasttestylasttestylast' }

        response = self.client.post('/pharma/contact', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------

    def test_create_fails_with_missing_username(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'email': 'testy01@gmail.com',
                        'firstname': 'firstname',
                        'surname': 'testylast' }

        response = self.client.post('/pharma/contact', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------

    def test_create_fails_with_missing_firstname(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'email': 'testy01@gmail.com',
                        'username': 'mename',
                        'surname': 'testylast' }

        response = self.client.post('/pharma/contact', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------

    def test_create_fails_with_missing_surname(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'email': 'testy01@gmail.com',
                        'username': 'mename',
                        'firstname': 'firsty' }

        response = self.client.post('/pharma/contact', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------

    def test_create_fails_with_missing_email(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'username': 'mename',
                        'firstname': 'firsty',
                        'surname': 'testylast' }

        response = self.client.post('/pharma/contact', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------

    def test_create_fails_with_duplicate_username(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'username': 'fred',
                        'email': 'fred4@email.com',
                        'firstname': 'fred',
                        'surname': 'bloggs' }

        response = self.client.post('/pharma/contact', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 409)

# -----------------------------------------------------------------------------

    def test_create_fails_with_duplicate_email(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'username': 'fred4',
                        'email': 'fred@email.com',
                        'firstname': 'fred',
                        'surname': 'bloggs' }

        response = self.client.post('/pharma/contact', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 409)

# -----------------------------------------------------------------------------

    def test_edit_passes_ok(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'username': 'fred',
                        'email': 'fred2@email.com',
                        'firstname': 'fred',
                        'surname': 'bloggs' }

        response = self.client.put('/pharma/contact/fred', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 200)

# -----------------------------------------------------------------------------

    def test_edit_fails_with_extra_field(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'username': 'fred2',
                        'email': 'fred2@email.com',
                        'firstname': 'fred',
                        'extra': 'bad',
                        'surname': 'bloggs' }

        response = self.client.put('/pharma/contact/fred', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------

    def test_edit_fails_with_extralong_username(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'username': 'fred2',
                        'email': 'fred2@email.com',
                        'firstname': 'fred',
                        'surname': 'bloggs' }

        response = self.client.put('/pharma/contact/fred123456789012345678901234567890', 
                                   json=create_json, headers=headers)
        self.assertEqual(response.status_code, 400)

# -----------------------------------------------------------------------------

    def test_edit_passes_ok_change_username(self):
        contacts = addTestContacts()
        headers = { 'Content-type': 'application/json' }
        create_json = { 'username': 'newfred',
                        'email': 'fred3@email.com',
                        'firstname': 'fred',
                        'surname': 'bloggs' }

        response = self.client.put('/pharma/contact/fred', json=create_json, headers=headers)
        self.assertEqual(response.status_code, 302)


