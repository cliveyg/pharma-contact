# app/tests/fixtures.py

from app import db
from app.models import Contact
import uuid
import os.path
import os
import csv
from sqlalchemy.exc import SQLAlchemyError

from dotenv import load_dotenv
load_dotenv()

# countries and addresses for tests

# -----------------------------------------------------------------------------

def addTestContacts():
    contact1 = Contact(username = "fred",
                       email = "fred@email.com",
                       surname = "bloggs",
                       firstname = "fred")
    contact2 = Contact(username = "jane",
                       email = "jane@email.com",
                       surname = "smith",
                       firstname = "jane")
    contact3 = Contact(username = "antôn",
                       email = "antôn@email.com",
                       surname = "deloitte",
                       firstname = "antôn")
    contact4 = Contact(username = "jim",
                       email = "jimi@email.com",
                       surname = "hooton",
                       firstname = "jimi")

    db.session.add(contact1)
    db.session.add(contact2)
    db.session.add(contact3)
    db.session.add(contact4)
    contacts = [contact1, contact2, contact3, contact4]
    db.session.commit()
    return contacts

