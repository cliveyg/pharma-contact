# app/models.py
from app import db

# -----------------------------------------------------------------------------

class Contact(db.Model):

    __tablename__ = 'contact'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30), unique=True, nullable=False)
    email = db.Column(db.String(150), unique=True, nullable=False)
    firstname = db.Column(db.String(50), unique=False, nullable=False)
    surname = db.Column(db.String(50), unique=False, nullable=False)

    def __init__(self, username, email, firstname, surname):
        self.username = username
        self.email = email
        self.firstname = firstname
        self.surname = surname

    def __repr__(self): # pragma: no cover 
        return '<id Contact {}>'.format(self.id)

# -----------------------------------------------------------------------------

