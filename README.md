# pharma test
This microservice is for adding, updating and deleting contacts.
I put 'pharma' in the URL as I was developing and testing this 
behind a web proxy on my own server.

Only accepts JSON inputs.

### API routes

```
/pharma/contact [GET]

Returns a list of all contacts. 
Possible return codes: [200, 500]

/pharma/contact [POST]

Needs the following values in the JSON body:
username (max 30 chars)
email (max 150 chars)
surname (max 50 chars)
firstname (max 50 chars)
Possible return codes: [201, 500]

/pharma/contact/<username> [DELETE]

Deletes the contact resource defined by the username in the URL. 
Possible return codes: [204, 400, 401]

/pharma/contact/<username> [GET]

Returns the contact resource defined by the username in the URL. 
Possible return codes: [200, 400, 404, 500]

/pharma/status [GET]

Returns a JSON message indicating system is running 
Possible return codes: [200]

/pharma/contact/<username> [PUT]

Allows editing of the username specified in the URL. Redirects if username is
changed.
Possible return codes: [200, 302, 400, 404, 500]

```

#### Notes:
Not PEP8 compliant. Not a fan of it. IMO it makes ugly hard to read code :)

#### Tests:
Tests can be run from app root using: 
`pytest --cov-config=app/tests/.coveragerc --cov=app app/tests`

Current test coverage is 100% (but see below)
I did have to get coverage to miss some things (using # pragma: no cover) as I
couldn't get SQLAlchemy/sqlite to error on cue. 

